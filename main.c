#include "MyPGP.h"

int main(int argc, char **argv)
{
	/*
	Déchhiffrer un fichier : 
	./PGP DECRYPT nomfichier clepriveeRSA
	
	Chiffrer un fichier :
	./PGP ENCRYPT nomfichier clepubliqueRSA
	./PGP ENCRYPT nomfichier clepubliqueRSA cleAES
	
	
	
	*/
	if(argc < 4 || argc > 5)
	{
		printf("	---------Utilisation du programme---------\n\n");
		printf(" Chiffer un fichier : \n");
		printf(" ./PGP ENCRYPT nom_fichier cle_privee_RSA\n\n");
		printf(" Déchiffer un fichier : \n");
		printf(" ./PGP DECRYPT nom_fichier cle_publique_RSA\n");
		printf(" ./PGP DECRYPT nom_fichier cle_publique_RSA cle_AES\n\n");
		exit(1);
	}
	
	/*CHIFFREMENT*/
	if( !strcmp(argv[1], "ENCRYPT") || !strcmp(argv[1], "encrypt")) 
	{
		if(argc!=4)
		{
			printf("Joindre le nom de fichier à chiffrer ainsi que la clé privée RSA\n");
			exit(1);
		}
		printf("Chiffrement du fichier\n");
		
		AES_KEY *key = aes_generate_key(128);
		
		RSA *RSAkey = load_pubkey(argv[3]);
		
		FILE *clair = fopen(argv[2],"rb");
		//FILE *cle = fopen(argv[3],"rb");
		
		FILE *chiffre = fopen("chiffre","wb");
		printf("Avant cypher \n");
		//pgp_cypher(key, RSAkey, clair, chiffre);
		
	}
	
	/*DECHIFFREMENT*/
	if( !strcmp(argv[1], "DECRYPT") || !strcmp(argv[1], "decrypt")) 
	{
		if(argc==4)
		{
			printf("Clé AES présente dans le chiffré\n");
		}
		if(argc==5)
		{
			printf("Clé AES en paramètre\n");
		}
	}
}
