#ifndef PGP_HEADER
#define PGP_HEADER

#include <stdlib.h>
#include <stdio.h>
#include "MyAES.h"
#include "my_RSA.h"

void pgp_cypher(AES_KEY *key, RSA *cli_pub_key, FILE *input, FILE *output);
void pgp_decypher(RSA *cli_private_key, FILE *input, FILE *output);

#endif
