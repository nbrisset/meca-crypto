all : main test_RSA
main : my_RSA.o MyAES.o MyPGP.o
	gcc -o PGP -g MyPGP.o MyAES.o my_RSA.o main.c -lcrypto
MyRSA.o : my_RSA.c my_RSA.h
	gcc -c -g my_RSA.c 
MyAES.o : MyAES.c MyAES.h
	gcc -c -g MyAES.c 
MyPGP.o : MyPGP.c MyPGP.h
	gcc -c -g MyPGP.c 
test_RSA.o: MyRSA.o
	gcc -c test_RSA.c
test_RSA: test_RSA.o
	gcc my_RSA.o test_RSA.o -o test_RSA.ex -lcrypto
clean :
	rm *.o PGP *~
