#include "MyPGP.h"

void pgp_cypher(AES_KEY *key, RSA *cli_pub_key, FILE *input, FILE *output)
{
	unsigned char cle_aes[16];
	unsigned char *cypher_key;
	long a;
	
	
	a = 128;
	/*
		on crée une clé AES => 128 bits
		on chiffre le message avec AES
		on chiffre la clé avec RSA => 1024 bits
		on colle la clé chiffrée en fin de message
		on ferme le fichier
	*/
	
	if(key == NULL)
		key = aes_generate_key(128);
	
	printf("aes_cypher \n");
	aes_cypher(input, output, key);
	
	memcpy(cle_aes, key->rd_key, 16);
	
	printf("rsa_encrypt \n");
	cypher_key = encrypt(cle_aes, cli_pub_key, &a);
	
	fseek(output, 0, SEEK_END);
	printf("fwrite \n");
	fwrite(cypher_key, 1, 128, output);	
}

void pgp_decypher(RSA *cli_private_key, FILE *input, FILE *output)
{
	unsigned char *cle_aes;
	unsigned char *cypher_key;
	AES_KEY key;
	long a = 128;
	cle_aes = (unsigned char *)malloc(16);
	
	/* 
		On déchiffre les 1024 derniers bits avec RSA
		On récupère la sortie pour obtenir la clé AES
		On déchiffre le tout avec la clé AES
		On ferme le fichier de sortie
	*/
	
	fseek(input, 0, SEEK_END);
	fseek(input, 0, -128);
	
	fread(cypher_key, 1, 128, input);
	
	cle_aes = decrypt(cypher_key, cli_private_key, a, 16);
	
	memcpy(key.rd_key, cle_aes, 16);
	key.rounds = 10;
	
	aes_decypher(input, output, &key);

}
