#ifndef RSA_HEADER
#define RSA_HEADER

#include <string.h>
#include <sys/stat.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>

void rsa_encrypt(const char *message, size_t message_length, RSA *cli_pub_key, char *encrypted_text, long *buffer_size);
void rsa_decrypt(const char *message, size_t message_length, RSA *cli_pri_key, char *decrypted_text, long decrypted_text_max_size);

RSA* load_pubkey(char *key_file_name);
RSA* load_privkey(char *key_file_name);

#endif
