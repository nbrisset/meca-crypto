#ifndef AES_HEADER
#define AES_HEADER
#include <openssl/rsa.h>
#include <openssl/engine.h>
#include <openssl/pem.h>
#include <openssl/bio.h>
#include <openssl/aes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void aes_cypher(FILE *input, FILE *output, AES_KEY *key);
void aes_decypher(FILE *input, FILE *output, AES_KEY *key);

unsigned char *aes_encrypt(char *message, const AES_KEY *key, size_t  *buffer_size);
unsigned char *aes_decrypt(char *message, const AES_KEY *key, size_t *buffer_size);

AES_KEY *aes_generate_key(unsigned short int size);
AES_KEY *aes_load_key(unsigned char *file_name);
AES_KEY *aes_retrieve_key(FILE *input);

#endif
