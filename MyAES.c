#include "MyAES.h"

void aes_cypher(FILE *input, FILE *output, AES_KEY *key){
	int i = 0, sz = 0, read_bytes = 0;
	unsigned char *m , *c;
	unsigned char iv[AES_BLOCK_SIZE];
  	memset(iv,0,AES_BLOCK_SIZE);

	fseek(input, 0L, SEEK_END);
	sz = ftell(input);
	fseek(input, 0L, SEEK_SET);
	m = (char*) calloc(sz, sizeof(char));
	c = (char*) calloc(sz + sz % AES_BLOCK_SIZE , sizeof(char));
	do{
		read_bytes = fread(m,1,AES_BLOCK_SIZE,input);
		printf("MyAES avant aes_encrypt\n");
		c = aes_encrypt(m,key,0);
		fwrite(c,1,AES_BLOCK_SIZE,output);
		printf("MyAES après le fwrite aes_cypher\n");
	}while(read_bytes == AES_BLOCK_SIZE);

	//free(m);
	//free(c);
}

void aes_decypher(FILE *input, FILE *output, AES_KEY *key){
	int i = 0, sz = 0, read_bytes = 0;
	unsigned char *m , *c;
	unsigned char iv[AES_BLOCK_SIZE];
  	memset(iv,0,AES_BLOCK_SIZE);

	fseek(input, 0L, SEEK_END);
	sz = ftell(input);
	fseek(input, 0L, SEEK_SET);
	m = (char*) calloc(sz, sizeof(char));
	c = (char*) calloc(sz + sz % AES_BLOCK_SIZE , sizeof(char));
	
	do{
		read_bytes = fread(m,1,AES_BLOCK_SIZE,input);
		c= aes_decrypt(m,key,0);
		fwrite(c,1,AES_BLOCK_SIZE,output);
	}while(read_bytes == AES_BLOCK_SIZE);

	//free(m);
	//free(c);
}

unsigned char *aes_encrypt(char *message, const AES_KEY *key, size_t  *buffer_size){
	unsigned char *c = (unsigned char*) calloc(AES_BLOCK_SIZE, sizeof(unsigned char));
	unsigned char iv[AES_BLOCK_SIZE];
  	memset(iv,0,AES_BLOCK_SIZE);
	printf("MyAES avant AES_cbc_encrypt\n");
	AES_cbc_encrypt(message, c ,128, key, iv, AES_ENCRYPT);
	printf("MyAES après AES_cbc_encrypt\n");
	return c;
}

unsigned char *aes_decrypt(char *message, const AES_KEY *key, size_t *buffer_size){
	unsigned char *c = (unsigned char*) calloc(AES_BLOCK_SIZE, sizeof(unsigned char));
	unsigned char iv[AES_BLOCK_SIZE];
  	memset(iv,0,AES_BLOCK_SIZE);

	AES_cbc_encrypt(message, c ,128, key, iv, AES_DECRYPT);
	return c;
}


AES_KEY *aes_generate_key(unsigned short int size)
{
	int i = 0;
	char key[128];
	AES_KEY *aes = (AES_KEY*) calloc(1, sizeof(AES_KEY));
	RAND_seed(key, size);
	
	do{
		i = RAND_bytes(key, sizeof key);
	}while(i == 0);

	AES_set_encrypt_key(key,128,aes);

	return aes;
}

AES_KEY *aes_load_key(unsigned char *file_name){
	FILE *file;
	file = fopen(file_name,"rb");
	aes_retrieve_key(file);
	fclose(file);
}

AES_KEY *aes_retrieve_key(FILE *input){
	char key[128];
	AES_KEY *aes = (AES_KEY*) calloc(1, sizeof(AES_KEY));

	fseek(input, 0L, SEEK_END);
	fseek(input, 0L, -128);
	fread(key, 1, 128,input);

	AES_set_encrypt_key(key, 128, aes);

	return aes;
}
