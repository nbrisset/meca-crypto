#include "MyRSA.h"

void rsa_encrypt(const char *message, size_t message_length, RSA *cli_pub_key, char *encrypted_text, long *buffer_size){

	*buffer_size = RSA_public_encrypt(message_length, (unsigned char *) message, encrypted_text, cli_pub_key, RSA_PKCS1_PADDING);
	if(*buffer_size == -1)
	{
		fputs("\n Impossible de chiffrer. \n",stderr);
		return;
	}
}
void rsa_decrypt(const char *message, size_t message_length, RSA *cli_pri_key, char *decrypted_text, long decrypted_text_max_size){
	long buffer_size = 0;
	unsigned char *decrypted;

	int n = RSA_private_decrypt(message_length, (unsigned char *) message, decrypted_text , cli_pri_key, RSA_PKCS1_PADDING);
	if(n == -1)
	{
		fputs("\n Impossible de déchiffrer. \n",stderr);
		return;
	}
}

RSA* load_pubkey(char *key_file_name){
	FILE *keyfile;
	struct stat file_status;
	char *file_buffer = NULL;
	long file_size;
	BIO *bp;
	RSA *pub_key;

	keyfile = fopen(key_file_name, "r");
	if(keyfile == NULL)
	{
		fputs("Echec du chargement de la clef!\n",stderr);
	}

	/* Retrieve the size of the file */
	stat(key_file_name, &file_status);
	file_size = file_status.st_size;
	file_buffer = (char *)malloc(file_size); /* Allocate memory for the buffer using the size of the file */
	fread(file_buffer,1,file_size,keyfile);
	fclose(keyfile);

	bp = BIO_new_mem_buf(file_buffer,-1);
	pub_key = PEM_read_bio_RSA_PUBKEY(bp,0,0,0);
	BIO_free(bp);

	free(file_buffer);
	return pub_key;
}
RSA* load_privkey(char *key_file_name){
	FILE *priv_key = fopen(key_file_name,"r");
	RSA *rsa_priv_key = PEM_read_RSAPrivateKey(priv_key,NULL,NULL,NULL);
	return rsa_priv_key;
}
