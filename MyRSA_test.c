#include "MyRSA.h"

int main (int argc, char **argv)
{

	int eflag = 0:
	int dflag = 0;


	while ((c = getopt (argc, argv, "ed")) != -1)
	{
		switch (c)
		{
			case 'e':
			eflag = 1;
			break;
			case 'd':
			dflag = 1;
			break;
			default:
			abort ();
		}
	}

	if(eflag){
		
	}

	RSA *pub_key  = load_pubkey("rsa_key.pub");	/*Load the public key*/
	RSA *rsa_priv_key = load_privkey("rsa_key.priv"); /*Load the private key*/

	char *message = "ABCDEFG";
	printf("Message clair : %s\n", message);
	long enc_message_length = 0;

	/* Encrypt the message! */
	unsigned char *encrypted = encrypt(message, pub_key, &enc_message_length); //The length of the encrypted buffer is stored in enc_message_length;
	printf("Message chiffre: %s\n", encrypted);

	/* Decrypt the message & print ! */
	unsigned char *decrypted = decrypt(encrypted, rsa_priv_key, enc_message_length, strlen(message));
	printf("Message dechiffre : %s\n",decrypted);

}